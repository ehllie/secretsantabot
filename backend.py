import discord
import simplecrypt
import functools
import random
import configparser


def clean(func):
    @functools.wraps(func)
    def wrapper(self, s):
        stripped = str(func(self, s))
        stripped = stripped.strip("b")
        stripped = stripped.strip('"')
        stripped = stripped.strip("'")
        return stripped
    return wrapper


class Person:

    def __init__(self, user: discord.User):
        self.id = user.id
        self._dummy_cipher = None
        self._contact_info = None
        self._wish = None
        self._other = None

    def __eq__(self, other):
        if isinstance(other, Person):
            return self.id == other.id
        elif isinstance(other, int):
            return self.id == other
        else:
            return False

    def __hash__(self):
        return int(self.id)

    def set_passwd(self, passwd):
        self._dummy_cipher = simplecrypt.encrypt(passwd, 'dummy')
        self._contact_info = None
        self._wish = None
        self._other = None

    def _has_passwd(func):
        @functools.wraps(func)
        def wrapper(self, passwd, *args):
            if self._dummy_cipher is None:
                self.set_passwd(passwd)
            return func(self, passwd, *args)
        return wrapper

    @_has_passwd
    def set_contact(self, passwd, *args):
        simplecrypt.decrypt(passwd, self._dummy_cipher)
        data = " ".join(args)
        self._contact_info = simplecrypt.encrypt(passwd, data)

    @_has_passwd
    def set_wish(self, passwd, *args):
        simplecrypt.decrypt(passwd, self._dummy_cipher)
        data = " ".join(args)
        self._wish = simplecrypt.encrypt(passwd, data)

    @_has_passwd
    def set_other(self, passwd, *args):
        simplecrypt.decrypt(passwd, self._dummy_cipher)
        data = " ".join(args)
        self._other = simplecrypt.encrypt(passwd, data)

    @clean
    def get_contact(self, passwd):
        if self._contact_info is not None:
            return simplecrypt.decrypt(passwd, self._contact_info)
        else:
            return None

    @clean
    def get_wish(self, passwd):
        if self._wish is not None:
            return simplecrypt.decrypt(passwd, self._wish)
        else:
            return None

    @clean
    def get_other(self, passwd):
        if self._other is not None:
            return simplecrypt.decrypt(passwd, self._other)
        else:
            return None


class BackendError(Exception):
    def get_name(self):
        pass


class SelectError(BackendError):
    def __init__(self):
        self.name = "SelectError"
        super().__init__()

    def get_name(self):
        return self.name


class ParticipationError(BackendError):
    def __init__(self):
        self.name = "ParticipationError"
        super().__init__()

    def get_name(self):
        return self.name


class BannedError(BackendError):
    def __init__(self):
        self.name = "BannedError"
        super().__init__()

    def get_name(self):
        return self.name


class StateError(BackendError):
    def __init__(self):
        self.name = "StateError"
        super().__init__()

    def get_name(self):
        return self.name


class NotFoundError(BackendError):
    def __init__(self):
        self.name = "NotFoundError"
        super().__init__()

    def get_name(self):
        return self.name


class NoServerError(BackendError):
    def __init__(self):
        self.name = "NoServerError"
        super().__init__()

    def get_name(self):
        return self.name


class Exchange:

    def __init__(self, exchange_nr, owner: discord.User, password=None):
        self._owner = owner
        self._id = exchange_nr
        self._participants = list()
        self._mods = set()
        self._mods.add(self._owner)
        self._help_channels = set()
        self._banned = set()
        self._open = False
        self._started = False
        self._assignments = dict()
        if password is None:
            self._lock = None
        else:
            self._lock = simplecrypt.encrypt(password, exchange_nr+password)

    def __eq__(self, other):
        if isinstance(other, Exchange):
            return self._id == other._id
        elif isinstance(other, int):
            return self._id == other
        else:
            return False

    def __hash__(self):
        return int(self._id)

    def has(self, user: discord.User):
        return user in self._participants

    def join(self, user: discord.User, password=None):
        if self._lock is not None:
            simplecrypt.decrypt(password, self._lock)
        if user.id in self._banned:
            raise BannedError
        elif not self._open:
            raise StateError
        elif user.id in self._participants:
            raise ParticipationError
        else:
            self._participants.append(Person(user))

    def leave(self, user: discord.User):
        if self._started:
            raise StateError
        elif user.id not in self._participants:
            raise ParticipationError
        else:
            self._participants.remove(user.id)

    def ban(self, user: discord.User):
        self.leave(user)
        self._banned.add(user.id)

    def _auto_assign(self):
        random.shuffle(self._participants)
        ln = len(self._participants)
        for i in range(ln):
            key = self._participants[i]
            val = self._participants[i]
            self._assignments[key] = val
        self._assignments[ln-1] = self._assignments[0]

    def get_assignment(self, user: discord.User):
        if user.id not in self._participants:
            raise ParticipationError
        elif self._open:
            raise StateError
        else:
            return self._assignments.get(user.id)

    def get_santa(self, user: discord.User):
        if user.id not in self._participants:
                raise ParticipationError
        elif self._open:
            raise StateError
        else:
            for participant in self._participants:
                if self._assignments[participant] == user.id:
                    return participant
        raise NotFoundError

    def swap(self, user_1: discord.User, user_2: discord.User):
        assign_1 = self.get_assignment(user_1)
        assign_2 = self.get_assignment(user_2)
        user_1 = user_1.id
        user_2 = user_2.id
        if assign_1 == user_2 or assign_2 == user_1:
            raise SelectError
        else:
            self._assignments[user_1] = assign_2
            self._assignments[user_2] = assign_1

    def set_help(self, channel: discord.TextChannel):
        if channel.id in self._help_channels:
            self._help_channels.remove(channel.id)
            raise StateError
        else:
            self._help_channels.add(channel.id)

    def set_mod(self, user):
        if user.id in self._mods:
            self._mods.remove(user.id)
            raise StateError
        else:
            self._mods.add(user.id)

    def get_help(self):
        return self._help_channels

    def get_mods(self):
        return self._mods

    def get_user(self, user: discord.User):
        for person in self._participants:
            if user.id == person:
                return person
        raise ParticipationError


def get_locale(cmd, err=None):
    parser = configparser.ConfigParser()
    parser.read("locale.txt")
    if err is None:
        cmd = "{}'s declaration arguments".format(cmd)
        return dict(parser.items(cmd))
    else:
        cmd = "{}'s error responses".format(cmd)
        return parser.get(cmd, err)


def get_func_names():
    functions = ["join", "leave", "mycontact", "mywish", "passwd", "msgsanta", "msgassign", "sendinfo", "msgmod",
                 "blocksanta", "blockassign", "create", "start", "open", "close", "inspect", "msgall", "modchnl",
                 "helpchnl", "swap", "kick", "ban"]
    for func in functions:
        yield func


errors = {ParticipationError, StateError, BannedError, SelectError, NotFoundError, NoServerError}
functions = {"join", "leave", "mycontact", "mywish", "passwd", "msgsanta", "msgassign", "sendinfo", "msgmod",
             "blocksanta", "blockassign", "start", "open", "close", "inspect", "msgall", "modchnl", "helpchnl",
             "swap", "kick", "ban"}

kwargs = {"name", "help", "brief", "description", "usage"}

parser = configparser.ConfigParser()
for func in functions:

    section = "{}'s error responses".format(func)
    contents = dict()
    contents["Success"] = "Response to Success form {} command".format(func)
    contents["DecryptionException"] = "Response to DecryptionException from {} command".format(func)
    contents["PermissionError"] = "Response to PermissionError from {} command".format(func)
    for err in errors:
        field = err.__name__
        contents[field] = "Response to {} from {} command".format(field, func)
    parser[section] = contents

    section = "{}'s declaration arguments".format(func)
    contents = dict()
    for arg in kwargs:
        if arg == "name":
            val = func
        else:
            val = "{}'s {}".format(section, arg)
        contents[arg] = val
    parser[section] = contents

with open("locale.txt", "w") as ofile:
    parser.write(ofile)

